# Le Libre et l'ESS <small>peuvent-ils faire bon ménage?</small>

Le Libre et l'ESS (Économie Sociale et Solidaire) partagent de nombreux aspects, en particulier la théorie des communs, et pourtant les deux courants peinent à faire converger leurs forces.

Petit tour d'horizon pour voir, ensemble, comment on pourrait améliorer le monde de l'ESS grâce au Libre, et améliorer le monde du Libre grâce à l'ESS.

[Support](./Libre-et-ESS.md)

## Générer le support

La source de ce support est en markdown, [reveal-md](https://github.com/webpro/reveal-md) permet de générer un support en `html` ou en `pdf`

### En html

```bash
reveal-md  Libre-et-ESS.md -w
```

### En pdf

```bash
reveal-md --print Libre-et-ESS.md
```
