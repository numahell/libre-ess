---
title: Le Libre et l'ESS
summary:
  Le Libre et l'ESS (Économie Sociale et Solidaire) partagent de nombreux aspects, en particulier la théorie des communs, et pourtant les deux courants peinent à faire converger leurs forces.
  Petit tour d'horizon pour voir, ensemble, comment on pourrait améliorer le monde de l'ESS grâce au Libre, et améliorer le monde du Libre grâce à l'ESS.
transition: slide
theme: solarized
---

# Le Libre et l'ESS <small>peuvent-ils faire bon ménage?</small>

----

## Qui sommes-nous ?

* **Élise Ghienne**, Interprète LSF et gérante de la SCOP Interpretis
* **Emmanuelle Helly**, développeuse chez Makina Corpus, militante à Framasoft, Alternatiba Toulouse

---

## C'est quoi l'ESS ?

----

> Loi 2014: 

* du point de vue économique : le partage des bénéfices ne peut pas être le seul but de l'activité, et une partie des bénéfices doit être réinvestie dans la structure (réserves impartageables)

* gouvernance démocratique (participation des associéEs, les salariéEs et des parties prenantes à l'activité)

----

* Associations : 78%
* Coopératives : 13%
* Mutuelles : 6%
* Fondations : 3%

> Mais aussi : 
* 10% du PIB national
* 2,4 millions de salariéEs
* 22 millions de bénévoles
* tous les secteurs d'activités et toutes les tailles

Note:

À la base du modèle économique : l'utilité sociale de l'activité.
https://en.wikipedia.org/wiki/Business_Model_Canvas#/media/File:Business_Model_Canvas.png

----

Au quotidien, cela veut dire partager

* des valeurs
* une vision
* un projet
* une organisation

----

> c'est pas toujours facile!

<img src="./images/questions-marion-orgueil.jpg" height="360px" >

<small>Dessin : Marion Orgueil</small>

----

Cela demande de 
* s'approprier la notion de propriété privée collective
* et de questionner la démocratie de la gouvernance

----

Bref, pratiquer la coopération, et pas seulement la collaboration

<img src="./images/cooperation-marion-orgueil.jpg" height="360px" />

<small>Dessin : Marion Orgueil</small>

----

### 2 visions de l'ESS:

* objectif de réparation et compensation les inégalités, mais sans remise en cause du systéme dominant

* objectif d'émancipation et d'auto-organisation pour sortir du système qui génère les inégalités.

Note:

Organisation horizontale ou hierarchisée pas seul critère, surtout sur l'objectif.

----

### Les Communs ?

Une **communauté** qui s'organise autour d'une ou plusieurs **ressources**, et qui édictent les **règles** permettant de partager et pérenniser ces ressources.

Note:

Notion de propriété collective
Enclosures

Théorisé par Elinor Ostrom

---

## C'est quoi le Libre ?

----

### Les 4 libertés


Un logiciel libre = 
des droits pour :

1. l'Utiliser
2. l'Étudier
3. Copier et distribuer
    les copies
4. Modifier et redistribuer
    les modifications
----

<img src="http://www.lilianricaud.com/travail-en-reseau/wp-content/uploads/2014/04/4-libertes-logiciel-libre.png" height="520" />

----

### Quelques chiffres

Open Source :

- 4 Milliards d'Euros
- 50 000 emplois

<small>Source: [Le logiciel libre représente 50 000 emplois et 4 milliards d'euros en France , JDN, 2015](https://www.journaldunet.com/solutions/dsi/1166651-le-secteur-du-logiciel-libre-represente-50-000-emplois-en-france/)</small>

> Où sont les chiffres sur le travail de contribution au logiciel libre ?

----

### Le libre = open source + des valeurs

> « Liberté, Égalité, Fraternité »

* La communauté, point essentiel pour faire d'un Logiciel Libre un Commun
* Contribuer à un logiciel libre, (presque) contribuer à un Commun
* Diversité des projets libres

Note:

Richard Stallmann, fondateur de la licence libre GNU GPL qui a lancé le mouvement du Logiciel Libre, a coutume de comparer le Logiciel Libre à la devise française, « Liberté, Égalité, Fraternité ». « Liberté » pour le respect des libertés des utiliseur⋅ices, « Égalité » car le Logiciel Libre ne permet pas à quelqu'un d'avoir du pouvoir sur une autre personne, « Fraternité » car la coopération et la contribution sont encouragées.

La communauté, son organisation, et la possibilité de contribuer est le point essentiel pour faire d'un Logiciel Libre un Commun. Contribuer à un Logiciel Libre, c'est contribuer à un Commun.

Entre le noyau linux, Nextcloud ou le logiciel Cagette qui permett de gérer une AMAP, il existe une grande diversité de projets libres, de même que pour l'ESS.

----

### Au-delà du Logiciel

* Communs de la connaissance : [Wikipedia](https://fr.wikipedia.org), [Telabotanica](https://www.tela-botanica.org/)
* Création et culture libre : licences [Creative Commons](https://creativecommons.org/)
* OpenData : [OpenStreetMap](https://www.openstreetmap.org/), [OpenFoodFacts](https://fr.openfoodfacts.org/decouvrir)

----

* Matériel libre : [Arduino](https://www.arduino.cc/), [Design libre Entropie](https://www.asso-entropie.fr/fr/design-libre/le-monde-du-libre/), [matériel agricole de l'Atelier Paysan](https://www.latelierpaysan.org/Plans-et-Tutoriels) (presque libre, en CC-By-SA-NC)
* Internet : FAI associatifs ([FFDN](https://www.ffdn.org/)), Hébergement web ([collectif CHATONS](https://chatons.org/))

La renaissance des **Communs**

Note:

On retrouve le Libre au-délà du logiciel seul. Les communs de la connaissance, dont l'exemple le plus connu est Wikipedia.
La création et la culture libre grâce aux licences Creative Commons.

L'OpenData, par exemple OpenStreetMap, base de données géographiques libre ou OpenFoodFacts, base de données libre des aliments.
Le matériel libre par exemple le téléphone Fairphone ou le micro-controlleur Arduino, ou encore du matériel agricole.

Enfin pour favoriser la décentralisation et la neutralité du net, avec la fédération des fournisseurs d'accès Internet associatifs (FFDN) et le Collectif des Hébergeurs Alternatifs Transparents Ouverts Neutres et Solidaires (CHATONS)

----

<img src="https://www.gwennseemel.com/images/2012/2012CANPenguin.jpg" height="520" />

<small>Peinture : [Gwenn Seemel](https://www.gwennseemel.com/artwork/), CC-0</small>

----

### Points d'attention

L'Open Source est partout, mais les logiciels ayant une utilité sociale peinent à être financés.

Contribuer à un projet libre : pas si simple

Ergonomie, design et expérience utilisateurs pas toujours pris en compte

Note:

L'Open Source est partout, mais les logiciels ayant une utilité sociale peinent à être financés (ou alors par le biais de startup qui n'en feront pas un Logiciel Libre, cf la Civic Tech). Lobbying très fort de Microsoft, Google vers l'État.

Contribuer à un projet libre n'est pas si simple. Même si'l est possible de contribuer de différentes manières (traduction, tests, documentation), les communautés qui organisent les contributions ne sont pas toujours très accueillantes, et les règles de contributions pas toujours explicites. Rapports de domination : les personnes là depuis longtemps dans la communauté qui acceptent difficilement les nouvelles contributions, les suggestions. Accepter qu'il y ait débat.

Savoirs complexes et spécifiques, on ne peut plus dire "fais le toi si t'es pas content".

Il manque à bon nombre de logiciels libres une interface ergonomique et agréable à utiliser. C'est le travail des ergonomes, des UX designers et des graphistes, et ils sont peu nombreux à contribuer aux logiciels libres pour de nombreuses raisons (mais c'est en progrès).


---

## Logiciel libre et ESS ?

Nous avons des valeurs communes !

----

### Le Libre a besoin de l'ESS ?

* Organiser une communauté autour d'un projet libre
→ statuts juridiques, modèles organisationnels démocratique de l'ESS
* Comment rétribuer les contributions
* Développer des logiciels ayant une utilité sociale, environnementale
→ l'ESS permet d'apporter du sens au travail

Note:

Il existe une variété des statuts juridiques permettant de créer des structures pour organiser la communauté des utilisateur⋅ices et contributeur⋅ices autour d'un projet libre, et pour financer les contributions. 

Toutes les contributions méritent une rétribution. Lorsqu'on contribue à un outil qui serve à la communauté, l'outil peut être payant même s'il est libre. Notion de propriété privée collective.

Si vous cherchez du sens à votre travail, en travaillant pour l'ESS vous aurez plus de chance de développer des logiciels ayant une utilité sociale et environnementale.

----

### L'ESS a besoin du Libre

* Se défaire des outils des GAFAM
* Formation aux outils libres existants
* Des logiciels de collaboration
* Développements spécifiques
* Ressources libres autres que logiciel

Note:

GAFAM et autres : ce qu'on apprend à l'école

Logiciels de collaboration déjà bien utilisés, mais méfiance : ≠ entre coopératif et collaboratif (pas de décision démocratique). Ex : ≠ la Ruche qui dit Oui et les AMAPs.

Plateformes collaboratives : répond à un besoin social, mais comment renverser la peversion. Éducation populaire / politique.

Ressources libres : mettre les ressources à disposition, sous licence libre, mettre en valeur ce que les associations / ESS produisent et les fait connaître. Question d'éducation, de choix de modèle économique.  
Une règle dans l'ESS : on ne peut jamais copier coller, il faut s'inspirer et adapter.

----

### Difficultés pour se rencontrer

*Plutôt des questionnements que des affirmations*

* Inquiétude et méfiance vis à vis du numérique
* Paradoxalement, utilisation massive des outils proposés par les GAFAM 
* Méconnaissance réciproque
    * "jargon" informatique, 30 ans de non-enseignement à l'école
    * ESS peu enseignée non plus

Note:

Méfiance vis à vis du numérique, et pourtant une utilisation massive des outils proposés par les GAFAM, car pratiques, ergonomiques, faciles à utiliser.  
Inquiétude par manque de maîtrise, donc méfiance. Confort vs effort, tout est prémaché alors qu'il faut apprendre des choses dans le logiciel libre.

Le monde du numérique méconnaît l'ESS, et le monde de l'ESS méconnaît le numérique et le libre en particulier. Le jargon informatique, et 30 ans de non-enseignement de l'informatique à l'école n'aide pas. En parallèle, on ne peut pas dire que l'économie sociale et solidaire soit mise en avant dans les média, par rapport à l'économie orthodoxe.

L'école enseigne la démocratie d'une certaine manière, celle des institutions, mais il existe plusieurs formes de démocraties. Cela nuit à tous, mais les personnes dans l'ESS découvrent ces autres formes.

----

* Manque d'adéquation des moyens / besoins de développement : développer un logiciel coûte cher
* Difficulté d'exprimer les besoins
* Manque d'adéquation des outils libres existants et des besoins

Note:

Développer un logiciel coûte cher. Vous avez un budget de 20 000€, déjà conséquent et que vous avez eu de la peine à réunir : si vous espérez avoir toutes les fonctionnalités de Facebook, vous serez déçu⋅e.

> Combien est assez ?

Rechercher à mutualiser les ressources pour se payer un logiciel.

Les outils libres existants ne sont pas les plus faciles d'utilisation, il y a du travail de design, d'UX, de formation.

----

### Des exemples des deux mondes

- Mobicoop, covoiturage libre ; Coopcycle, livraisons à vélo
- Services et formations en informatique : Scopyleft, Human coders, Dalibo
- CAE, proche du portage salarial : Coopaname
- Associations ayant une forte utilité sociale : [Emmabuntu](https://emmabuntus.sourceforge.io/mediawiki/) par Emmaüs, Linux pour les écoles au Mali ([Bilou Toguna](https://capitoledulibre.org/programme/#afrikalan-des-pinguoins-sur-les-bancs-des-ecoles-m))

---

## Forum régional de l'ESS<br>Mois de l'ESS

![](http://www.fress-occitanie.fr/wp-content/uploads/2019/08/bandeau.jpg)

---

## Discussion ouverte

C'est à vous :)

Note:

Et vous ? est-ce que vous vous reconnaissez dans ce que nous décrivons ?

---

## Sketchnote par Ervin

Merci à Ervin pour la sketchnote réalisée pendant notre présentation !

[Voir ses sketchnotes réalisées pendant le Capitole du Libre](https://ervin.ipsquad.net/blog/2019/11/17/sketchnotes-at-capitole-du-libre-2019/)

----

<img alt="sketchnote" src="https://ervin.ipsquad.net/photos/cdl-2019/05-LLetESS.png" height="520" />
